import React, { useEffect, useState } from "react";
import axios from "axios";
import "./App.css";

function App() {
  const [data, setData] = useState();
  const [files, setFiles] = useState([]);

  function handleChange(e) {
    setData(e.target.files[0]);
  }

  useEffect(() => {
    getAllData();
  }, [files]);

  const handleSubmit = (e) => {
    e.preventDefault();
    const formData = new FormData();
    formData.append("file", data);

    const URL = "http://localhost:9001/file";
    const config = {
      Headers: {
        contentType: "multipart/form-data",
      },
    };

    // const res = fetch("http://localhost:9001/file", {
    //   mode: "no-cors",
    //   method: "POST",
    //   body: formData,
    // }).then((res) => res.json());
    // alert(JSON.stringify(`${res.message}, status: ${res.status}`));

    axios
      .post(URL, formData, config)
      .then((res) => {
        alert("file uploaded successfully");
      })
      .catch((err) => {
        alert(err);
      });
  };

  const getAllData = () => {
    const URL = "http://localhost:9001/file/all";
    axios.get(URL).then((res) => {
      setFiles(res.data);
    });
  };

  const downloadFile = (fileName) => {
    const URL = "http://localhost:9001/file/" + fileName;

    axios({
      url: URL,
      method: "GET",
      responseType: "blob",
    })
      .then((res) => {
        console.log(res);
        const url = window.URL.createObjectURL(new Blob([res.data]));
        const link = document.createElement("a");
        link.href = url;
        link.setAttribute("download", fileName);
        document.body.appendChild(link);
        link.click();
      })
      .catch((err) => {
        console.log(err);
      });
  };

  const deleteFile = (fileName) => {
    const URL = "http://localhost:9001/file/" + fileName;

    axios({
      url: URL,
      method: "DELETE",
    })
      .then((res) => {
        alert("file deleted successfully");
      })
      .catch((err) => {
        alert(err);
      });
  };

  return (
    <div className="App">
      <form>
        <input type="file" onChange={handleChange} />

        <input type="submit" onClick={handleSubmit} />
      </form>

     {files.length>0 && <table className="file-table">
        <thead>
          <tr>
            <th>Sl No</th>
            <th>name</th>
            <th>type</th>
            <th>download</th>
            <th>delete</th>
          </tr>
        </thead>
        <tbody>
          {files.map((file, i) => {
            return (
              <tr key={i}>
                <td>{i + 1}</td>
                <td>{file.name}</td>
                <td>{file.type}</td>
                <td>
                  <button onClick={() => downloadFile(file.name)}>
                    download
                  </button>
                </td>
                <td>
                  {" "}
                  <button onClick={() => deleteFile(file.name)}>delete</button>
                </td>
              </tr>
            );
          })}
        </tbody>
      </table>}
    </div>
  );
}

export default App;
